package com.example.extra

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.navigation.fragment.NavHostFragment
import com.example.extra.databinding.FragmentGetStartedBinding

class GetStartedFragment : Fragment() {

    private var _binding: FragmentGetStartedBinding? = null

    // This property is only valid between onCreateView and
    // onDestroyView.
    private val binding get() = _binding!!

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _binding = FragmentGetStartedBinding.inflate(inflater, container, false)
        val view = binding.root

        binding.loginButton.setOnClickListener {
            NavHostFragment.findNavController(this)
                .navigate(R.id.action_getStartedFragment_to_signInFragment)
        }

        binding.registerButton.setOnClickListener {
            NavHostFragment.findNavController(this)
                .navigate(R.id.action_getStartedFragment_to_registerFragment)
        }

        binding.welcomeBackPage.setOnClickListener {
            NavHostFragment.findNavController(this)
                .navigate(R.id.action_getStartedFragment_to_welcomeBackFragment)
        }
        return view
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }

}