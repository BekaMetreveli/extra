package com.example.extra

import android.content.Context
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.view.KeyEvent
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.inputmethod.InputMethodManager
import android.widget.EditText
import android.widget.Toast
import androidx.core.content.res.ResourcesCompat
import androidx.navigation.fragment.NavHostFragment
import com.example.extra.databinding.FragmentCreatePasscodeBinding
import com.example.extra.databinding.FragmentSetAPasscodeBinding

class CreatePasscodeFragment : Fragment() {

    private var _binding: FragmentCreatePasscodeBinding? = null

    // This property is only valid between onCreateView and
    // onDestroyView.
    private val binding get() = _binding!!

    private lateinit var inputMethodManager: InputMethodManager

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _binding = FragmentCreatePasscodeBinding.inflate(inflater, container, false)
        val view = binding.root

        inputMethodManager =
            requireActivity().getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager

        return view
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        (activity as MainActivity?)!!.setSupportActionBar(binding.toolbar)
        (activity as MainActivity?)!!.supportActionBar?.setDisplayShowTitleEnabled(false)
        (activity as MainActivity?)!!.supportActionBar?.setDisplayHomeAsUpEnabled(true)
        binding.toolbar.setNavigationOnClickListener {
            requireActivity().onBackPressed()
        }
        binding.edit1.requestFocus()
        inputMethodManager.showSoftInput(binding.edit1, 0)

        class GenericTextWatcher(
            private val currentView: View,
            private val nextView: View?
        ) : TextWatcher {
            override fun afterTextChanged(editable: Editable) {
                val text = editable.toString()
                when (currentView) {
                    binding.edit1 -> if (text.length == 1) {
                        nextView!!.requestFocus()
                        currentView.background = ResourcesCompat.getDrawable(resources, R.drawable.edittext_round_filled_background, null)
                    }
                    binding.edit2 -> if (text.length == 1) {
                        nextView!!.requestFocus()
                        currentView.background = ResourcesCompat.getDrawable(resources, R.drawable.edittext_round_filled_background, null)
                    }
                    binding.edit3 -> if (text.length == 1) {
                        nextView!!.requestFocus()
                        currentView.background = ResourcesCompat.getDrawable(resources, R.drawable.edittext_round_filled_background, null)
                    }
                    binding.edit4 -> if (text.length == 1) {
                        currentView.background = ResourcesCompat.getDrawable(resources, R.drawable.edittext_round_filled_background, null)
                    }
                    //You can use EditText4 same as above to hide the keyboard
                }
//                currentView.background = ResourcesCompat.getDrawable(resources, R.drawable.edittext_round_filled_background, null)
            }

            override fun beforeTextChanged(s: CharSequence, start: Int, count: Int, after: Int) {}
            override fun onTextChanged(s: CharSequence, start: Int, before: Int, count: Int) {}
        }

        class GenericKeyEvent(
            private val currentView: EditText,
            private val previousView: EditText?
        ) : View.OnKeyListener {
            override fun onKey(p0: View?, keyCode: Int, event: KeyEvent?): Boolean {

                if (event!!.action == KeyEvent.ACTION_DOWN && keyCode == KeyEvent.KEYCODE_DEL && currentView != binding.edit1) {

                    if (currentView.text.isEmpty()) {
                        //If current is empty then previous EditText's number will also be deleted
                        previousView!!.text = null
                        previousView.background = ResourcesCompat.getDrawable(
                            resources,
                            R.drawable.edittext_round_background,
                            null
                        )
                        previousView.requestFocus()
                        return true
                    } else {
                        binding.edit4.background = ResourcesCompat.getDrawable(
                            resources,
                            R.drawable.edittext_round_background,
                            null
                        )

                    }
                }
                return false
            }

        }

        //GenericTextWatcher here works only for moving to next EditText when a number is entered
//first parameter is the current EditText and second parameter is next EditText
        binding.edit1.addTextChangedListener(GenericTextWatcher(binding.edit1, binding.edit2))
        binding.edit2.addTextChangedListener(GenericTextWatcher(binding.edit2, binding.edit3))
        binding.edit3.addTextChangedListener(GenericTextWatcher(binding.edit3, binding.edit4))
        binding.edit4.addTextChangedListener(GenericTextWatcher(binding.edit4, null))

//GenericKeyEvent here works for deleting the element and to switch back to previous EditText
//first parameter is the current EditText and second parameter is previous EditText
        binding.edit1.setOnKeyListener(GenericKeyEvent(binding.edit1, null))
        binding.edit2.setOnKeyListener(GenericKeyEvent(binding.edit2, binding.edit1))
        binding.edit3.setOnKeyListener(GenericKeyEvent(binding.edit3, binding.edit2))
        binding.edit4.setOnKeyListener(GenericKeyEvent(binding.edit4, binding.edit3))

        binding.nextButton.setOnClickListener {
            val sb = StringBuilder()
            sb.append(binding.edit1.text.toString())
            sb.append(binding.edit2.text.toString())
            sb.append(binding.edit3.text.toString())
            sb.append(binding.edit4.text.toString())
            Toast.makeText(requireContext(), sb, Toast.LENGTH_SHORT).show()

//            NavHostFragment.findNavController(this)
//                .navigate(R.id.action_enterDigitsFragment_to_passwordFragment)

        }

        binding.root.setOnClickListener {
            inputMethodManager.hideSoftInputFromWindow(
                requireActivity().currentFocus?.windowToken,
                0
            )
        }


    }

    override fun onDestroyView() {
        super.onDestroyView()
        inputMethodManager.hideSoftInputFromWindow(
            requireActivity().currentFocus?.windowToken,
            0
        )
        _binding = null
    }

}