package com.example.extra

import android.annotation.SuppressLint
import android.content.Context
import android.os.Bundle
import android.text.Editable
import android.text.InputType
import android.text.TextWatcher
import android.text.method.ScrollingMovementMethod
import android.view.*
import android.view.inputmethod.InputMethodManager
import androidx.core.content.res.ResourcesCompat
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.NavHostFragment
import com.example.extra.databinding.FragmentPasswordBinding


class PasswordFragment : Fragment() {

    private var _binding: FragmentPasswordBinding? = null

    // This property is only valid between onCreateView and
    // onDestroyView.
    private val binding get() = _binding!!

    private var hiddenInput = true
    private lateinit var inputMethodManager: InputMethodManager

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _binding = FragmentPasswordBinding.inflate(inflater, container, false)
        val view = binding.root
        inputMethodManager =
            requireActivity().getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager


        return view
    }

    @SuppressLint("ClickableViewAccessibility")
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        (activity as MainActivity?)!!.setSupportActionBar(binding.toolbar)
        (activity as MainActivity?)!!.supportActionBar?.setDisplayShowTitleEnabled(false)
        (activity as MainActivity?)!!.supportActionBar?.setDisplayHomeAsUpEnabled(true)
        binding.toolbar.setNavigationOnClickListener {
            requireActivity().onBackPressed()
        }

        binding.confirmPassword.setOnKeyListener(object : View.OnKeyListener {
            override fun onKey(v: View?, keyCode: Int, event: KeyEvent): Boolean {
                // If the event is a key-down event on the "enter" button
                if (event.action == KeyEvent.ACTION_DOWN &&
                    keyCode == KeyEvent.KEYCODE_ENTER
                ) {
                    inputMethodManager.hideSoftInputFromWindow(
                        requireActivity().currentFocus?.windowToken,
                        0
                    )
                    //Same as pressing button
                    return true
                }
                return false
            }
        })

        binding.password.setOnTouchListener(object : View.OnTouchListener {
            override fun onTouch(p0: View?, p1: MotionEvent?): Boolean {
                val DRAWABLE_LEFT = 0
                val DRAWABLE_TOP = 1
                val DRAWABLE_RIGHT = 2
                val DRAWABLE_BOTTOM = 3
                if (p1?.action == MotionEvent.ACTION_UP) {
                    if (p1.rawX >= binding.password.right - binding.password.compoundDrawables[DRAWABLE_RIGHT].bounds.width()
                    ) {
                        // your action here
                        if (hiddenInput) {
                            binding.password.inputType =
                                InputType.TYPE_CLASS_TEXT or InputType.TYPE_TEXT_VARIATION_VISIBLE_PASSWORD
                            binding.password.setCompoundDrawablesWithIntrinsicBounds(
                                null,
                                null,
                                ResourcesCompat.getDrawable(
                                    resources,
                                    R.drawable.ic_invisible,
                                    null
                                ),
                                null
                            );
                            hiddenInput = false
                        } else {
                            binding.password.inputType =
                                InputType.TYPE_CLASS_TEXT or InputType.TYPE_TEXT_VARIATION_PASSWORD
                            binding.password.setCompoundDrawablesWithIntrinsicBounds(
                                null,
                                null,
                                ResourcesCompat.getDrawable(resources, R.drawable.ic_eye, null),
                                null
                            );
                            hiddenInput = true
                        }
                        return true
                    }
                }
                return false
            }
        })

        binding.confirmPassword.setOnTouchListener(object : View.OnTouchListener {
            override fun onTouch(p0: View?, p1: MotionEvent?): Boolean {
                val DRAWABLE_LEFT = 0
                val DRAWABLE_TOP = 1
                val DRAWABLE_RIGHT = 2
                val DRAWABLE_BOTTOM = 3
                if (p1?.action == MotionEvent.ACTION_UP) {
                    if (p1.rawX >= binding.confirmPassword.right - binding.confirmPassword.compoundDrawables[DRAWABLE_RIGHT].bounds.width()
                    ) {
                        // your action here
                        if (hiddenInput) {
                            binding.confirmPassword.inputType =
                                InputType.TYPE_CLASS_TEXT or InputType.TYPE_TEXT_VARIATION_VISIBLE_PASSWORD
                            if (binding.confirmPasswordEditText.error == null) {
                                binding.confirmPassword.setCompoundDrawablesWithIntrinsicBounds(
                                    null,
                                    null,
                                    ResourcesCompat.getDrawable(
                                        resources,
                                        R.drawable.ic_eye,
                                        null
                                    ),
                                    null
                                )
                            } else {
                                binding.confirmPassword.setCompoundDrawablesWithIntrinsicBounds(
                                    null,
                                    null,
                                    ResourcesCompat.getDrawable(
                                        resources,
                                        R.drawable.ic_eye_error,
                                        null
                                    ),
                                    null
                                )
                            }

                            hiddenInput = false
                        } else {
                            binding.confirmPassword.inputType =
                                InputType.TYPE_CLASS_TEXT or InputType.TYPE_TEXT_VARIATION_PASSWORD
                            if (binding.confirmPasswordEditText.error == null) {
                                binding.confirmPassword.setCompoundDrawablesWithIntrinsicBounds(
                                    null,
                                    null,
                                    ResourcesCompat.getDrawable(resources, R.drawable.ic_invisible, null),
                                    null
                                )
                            } else {
                                binding.confirmPassword.setCompoundDrawablesWithIntrinsicBounds(
                                    null,
                                    null,
                                    ResourcesCompat.getDrawable(resources, R.drawable.ic_invisible_error, null),
                                    null
                                )
                            }

                            hiddenInput = true
                        }
                        return true
                    }
                }
                return false
            }
        })

        binding.confirmPassword.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
            }

            override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
                if (binding.confirmPassword.text.toString() != binding.password.text.toString()) {
                    binding.confirmPasswordEditText.error = "Doesn't match"
                    if (hiddenInput) {
                        binding.confirmPassword.setCompoundDrawablesWithIntrinsicBounds(
                            null,
                            null,
                            ResourcesCompat.getDrawable(
                                resources,
                                R.drawable.ic_invisible_error,
                                null
                            ),
                            null
                        )
                    } else {
                        binding.confirmPassword.setCompoundDrawablesWithIntrinsicBounds(
                            null,
                            null,
                            ResourcesCompat.getDrawable(
                                resources,
                                R.drawable.ic_eye_error,
                                null
                            ),
                            null
                        );
                    }
                } else {
                    binding.confirmPasswordEditText.error = null
                    if (hiddenInput) {
                        binding.confirmPassword.setCompoundDrawablesWithIntrinsicBounds(
                            null,
                            null,
                            ResourcesCompat.getDrawable(
                                resources,
                                R.drawable.ic_invisible,
                                null
                            ),
                            null
                        )
                    } else {
                        binding.confirmPassword.setCompoundDrawablesWithIntrinsicBounds(
                            null,
                            null,
                            ResourcesCompat.getDrawable(
                                resources,
                                R.drawable.ic_eye,
                                null
                            ),
                            null
                        );
                    }
                }
            }

            override fun afterTextChanged(p0: Editable?) {

            }

        })

        binding.termsTV.setOnClickListener {
            NavHostFragment.findNavController(this)
                .navigate(R.id.action_passwordFragment_to_termsFragment)
        }

        binding.createAccountButton.setOnClickListener {
            NavHostFragment.findNavController(this)
                .navigate(R.id.action_passwordFragment_to_turnOnBiometricLoginFragment)
        }


    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }

}