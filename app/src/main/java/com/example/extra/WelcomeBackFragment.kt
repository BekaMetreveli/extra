package com.example.extra

import android.Manifest
import android.app.KeyguardManager
import android.content.Context
import android.content.DialogInterface
import android.content.pm.PackageManager
import android.hardware.biometrics.BiometricPrompt
import android.os.Bundle
import android.os.CancellationSignal
import android.text.Editable
import android.text.TextWatcher
import android.view.KeyEvent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.inputmethod.InputMethodManager
import android.widget.EditText
import android.widget.Toast
import androidx.core.app.ActivityCompat
import androidx.core.content.res.ResourcesCompat
import androidx.fragment.app.Fragment
import com.example.extra.databinding.FragmentWelcomeBackBinding


class WelcomeBackFragment : Fragment() {

    private var _binding: FragmentWelcomeBackBinding? = null

    // This property is only valid between onCreateView and
    // onDestroyView.
    private val binding get() = _binding!!

    private lateinit var inputMethodManager: InputMethodManager

    private var cancellationSignal: CancellationSignal? = null
    private val authenticationCallback: BiometricPrompt.AuthenticationCallback
    get() = object : BiometricPrompt.AuthenticationCallback() {
        override fun onAuthenticationError(errorCode: Int, errString: CharSequence?) {
            super.onAuthenticationError(errorCode, errString)
            notifyUser("Authentication Error: $errString")
        }

        override fun onAuthenticationSucceeded(result: BiometricPrompt.AuthenticationResult?) {
            super.onAuthenticationSucceeded(result)
            notifyUser("Authentication Success!")
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _binding = FragmentWelcomeBackBinding.inflate(inflater, container, false)
        val view = binding.root

        inputMethodManager =
            requireActivity().getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager

        checkBiometricSupport()

        return view
    }

    private fun checkBiometricSupport(): Boolean {
        val keyguardManager: KeyguardManager = requireActivity().getSystemService(Context.KEYGUARD_SERVICE) as KeyguardManager

        if (!keyguardManager.isKeyguardSecure) {
            notifyUser("Fingerprint authentication is not enabled in settings.")
            return false
        }

        if (ActivityCompat.checkSelfPermission(requireContext(), Manifest.permission.USE_BIOMETRIC) != PackageManager.PERMISSION_GRANTED) {
            notifyUser("Fingerprint authentication is not enabled.")
            return false
        }

        return if (requireActivity().packageManager.hasSystemFeature(PackageManager.FEATURE_FINGERPRINT)) {
            true
        } else {
            true
        }
    }

    private fun getCancellationSignal(): CancellationSignal{
        cancellationSignal = CancellationSignal()
        cancellationSignal?.setOnCancelListener {
            notifyUser("Authentication was canceled")
        }
        return cancellationSignal as CancellationSignal
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        binding.edit1.requestFocus()
        inputMethodManager.showSoftInput(binding.edit1, 0)

        class GenericTextWatcher(private val currentView: View, private val nextView: View?) :
            TextWatcher {
            override fun afterTextChanged(editable: Editable) {
                val text = editable.toString()
                when (currentView) {
                    binding.edit1 -> if (text.length == 1) {
                        nextView!!.requestFocus()
                        currentView.background = ResourcesCompat.getDrawable(
                            resources,
                            R.drawable.edittext_round_filled_background,
                            null
                        )
                    }
                    binding.edit2 -> if (text.length == 1) {
                        nextView!!.requestFocus()
                        currentView.background = ResourcesCompat.getDrawable(
                            resources,
                            R.drawable.edittext_round_filled_background,
                            null
                        )
                    }
                    binding.edit3 -> if (text.length == 1) {
                        nextView!!.requestFocus()
                        currentView.background = ResourcesCompat.getDrawable(
                            resources,
                            R.drawable.edittext_round_filled_background,
                            null
                        )
                    }
                    binding.edit4 -> if (text.length == 1) {
                        currentView.background = ResourcesCompat.getDrawable(
                            resources,
                            R.drawable.edittext_round_filled_background,
                            null
                        )
                    }
                    //You can use EditText4 same as above to hide the keyboard
                }
//                currentView.background = ResourcesCompat.getDrawable(resources, R.drawable.edittext_round_filled_background, null)
            }

            override fun beforeTextChanged(s: CharSequence, start: Int, count: Int, after: Int) {}
            override fun onTextChanged(s: CharSequence, start: Int, before: Int, count: Int) {}
        }

        class GenericKeyEvent(
            private val currentView: EditText,
            private val previousView: EditText?
        ) : View.OnKeyListener {
            override fun onKey(p0: View?, keyCode: Int, event: KeyEvent?): Boolean {

                if (event!!.action == KeyEvent.ACTION_DOWN && keyCode == KeyEvent.KEYCODE_DEL && currentView != binding.edit1) {

                    if (currentView.text.isEmpty()) {
                        //If current is empty then previous EditText's number will also be deleted
                        previousView!!.text = null
                        previousView.background = ResourcesCompat.getDrawable(
                            resources,
                            R.drawable.edittext_round_background,
                            null
                        )
                        previousView.requestFocus()
                        return true
                    } else {
                        binding.edit4.background = ResourcesCompat.getDrawable(
                            resources,
                            R.drawable.edittext_round_background,
                            null
                        )

                    }
                }
                return false
            }

        }

        //GenericTextWatcher here works only for moving to next EditText when a number is entered
//first parameter is the current EditText and second parameter is next EditText
        binding.edit1.addTextChangedListener(GenericTextWatcher(binding.edit1, binding.edit2))
        binding.edit2.addTextChangedListener(GenericTextWatcher(binding.edit2, binding.edit3))
        binding.edit3.addTextChangedListener(GenericTextWatcher(binding.edit3, binding.edit4))
        binding.edit4.addTextChangedListener(GenericTextWatcher(binding.edit4, null))

//GenericKeyEvent here works for deleting the element and to switch back to previous EditText
//first parameter is the current EditText and second parameter is previous EditText
        binding.edit1.setOnKeyListener(GenericKeyEvent(binding.edit1, null))
        binding.edit2.setOnKeyListener(GenericKeyEvent(binding.edit2, binding.edit1))
        binding.edit3.setOnKeyListener(GenericKeyEvent(binding.edit3, binding.edit2))
        binding.edit4.setOnKeyListener(GenericKeyEvent(binding.edit4, binding.edit3))


//        val sb = StringBuilder()
//        sb.append(binding.edit1.text.toString())
//        sb.append(binding.edit2.text.toString())
//        sb.append(binding.edit3.text.toString())
//        sb.append(binding.edit4.text.toString())
//        Toast.makeText(requireContext(), sb, Toast.LENGTH_SHORT).show()

        binding.biometricButton.setOnClickListener {
            val biometricPrompt = BiometricPrompt.Builder(requireContext())
                .setTitle("Biometric Login for my app")
                .setSubtitle("Log in using your biometric credential")
                .setNegativeButton("Cancel", requireActivity().mainExecutor, DialogInterface.OnClickListener { dialogInterface, i ->
                    notifyUser("Authentication canceled.")
                }).build()

            biometricPrompt.authenticate(getCancellationSignal(), requireActivity().mainExecutor, authenticationCallback)
        }


    }

    private fun notifyUser(message: String) {
        Toast.makeText(requireContext(), message, Toast.LENGTH_SHORT).show()
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }

}