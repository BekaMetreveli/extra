package com.example.extra

import android.animation.Animator
import android.animation.AnimatorListenerAdapter
import android.annotation.SuppressLint
import android.content.Context
import android.os.Bundle
import android.text.InputType
import android.view.*
import android.view.inputmethod.InputMethodManager
import androidx.activity.addCallback
import androidx.core.content.res.ResourcesCompat
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.NavHostFragment
import com.example.extra.databinding.FragmentSignInBinding

class SignInFragment : Fragment() {

    private var _binding: FragmentSignInBinding? = null

    // This property is only valid between onCreateView and
    // onDestroyView.
    private val binding get() = _binding!!

    private var hiddenInput = true
    private lateinit var inputMethodManager: InputMethodManager

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        // This callback will only be called when MyFragment is at least Started.
        requireActivity().onBackPressedDispatcher.addCallback(this) {
            // Handle the back button event
            if (binding.emailOrMobile.hasFocus()) {
                binding.emailOrMobile.clearFocus()
//                binding.passwordEditText.startAnimation(anim)
                binding.consLayout.animate()
                    .translationY(0f)
                    .alpha(0.0f)
                    .setListener(object : AnimatorListenerAdapter() {
                        override fun onAnimationEnd(animation: Animator?) {
                            super.onAnimationEnd(animation)
                            binding.consLayout.visibility = View.GONE
                        }
                    })
            } else {
                isEnabled = false
                requireActivity().onBackPressed()
            }

        }
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _binding = FragmentSignInBinding.inflate(inflater, container, false)
        val view = binding.root
        inputMethodManager =
            requireActivity().getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager

        return view
    }

    @SuppressLint("ClickableViewAccessibility")
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        (activity as MainActivity?)!!.setSupportActionBar(binding.toolbar)
        (activity as MainActivity?)!!.supportActionBar?.setDisplayShowTitleEnabled(false)
        (activity as MainActivity?)!!.supportActionBar?.setDisplayHomeAsUpEnabled(true)
        binding.toolbar.setNavigationOnClickListener {
            requireActivity().onBackPressed()
        }
        binding.emailOrMobile.setOnFocusChangeListener { view, b ->
            if (b) {
                binding.consLayout.visibility = View.VISIBLE
                binding.consLayout.alpha = 1f
            }

        }

        binding.password.setOnKeyListener(object : View.OnKeyListener {
            override fun onKey(v: View?, keyCode: Int, event: KeyEvent): Boolean {
                // If the event is a key-down event on the "enter" button
                if (event.action == KeyEvent.ACTION_DOWN &&
                    keyCode == KeyEvent.KEYCODE_ENTER
                ) {
                    inputMethodManager.hideSoftInputFromWindow(
                        requireActivity().currentFocus?.windowToken,
                        0
                    )
                    //Same as pressing button
                    return true
                }
                return false
            }
        })

        binding.password.setOnTouchListener(object : View.OnTouchListener {
            override fun onTouch(p0: View?, p1: MotionEvent?): Boolean {
                val DRAWABLE_LEFT = 0
                val DRAWABLE_TOP = 1
                val DRAWABLE_RIGHT = 2
                val DRAWABLE_BOTTOM = 3
                if (p1?.action == MotionEvent.ACTION_UP) {
                    if (p1.rawX >= binding.password.right - binding.password.compoundDrawables[DRAWABLE_RIGHT].bounds.width()
                    ) {
                        // your action here
                        if (hiddenInput) {
                            binding.password.inputType =
                                InputType.TYPE_CLASS_TEXT or InputType.TYPE_TEXT_VARIATION_VISIBLE_PASSWORD
                            binding.password.setCompoundDrawablesWithIntrinsicBounds(
                                null,
                                null,
                                ResourcesCompat.getDrawable(
                                    resources,
                                    R.drawable.ic_invisible,
                                    null
                                ),
                                null
                            );
                            hiddenInput = false
                        } else {
                            binding.password.inputType =
                                InputType.TYPE_CLASS_TEXT or InputType.TYPE_TEXT_VARIATION_PASSWORD
                            binding.password.setCompoundDrawablesWithIntrinsicBounds(
                                null,
                                null,
                                ResourcesCompat.getDrawable(resources, R.drawable.ic_eye, null),
                                null
                            );
                            hiddenInput = true
                        }
                        return true
                    }
                }
                return false
            }
        })

        binding.forgotPasswordTextView.setOnClickListener {
            NavHostFragment.findNavController(this)
                .navigate(R.id.action_signInFragment_to_passwordRecovery1Fragment)
        }

        binding.root.setOnClickListener {
            inputMethodManager.hideSoftInputFromWindow(
                requireActivity().currentFocus?.windowToken,
                0
            )
        }

    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }

}